window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/states/";
  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error("Failed to fetch states url! Try again!");
    }

    const data = await response.json();

    const stateTag = document.getElementById("state");
    for (let state of data.states) {
      const option = document.createElement("option");
      option.value = state.abbreviation;
      option.innerHTML = state.name;
      stateTag.appendChild(option);
    }

    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener("submit", async (event) => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const locationUrl = "http://localhost:8000/api/locations/";
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
        }
      } catch (e) {
        console.error("failed to make new location!", e);
      }
    });
  } catch (e) {
    console.error("Failed to get state api!", e);
  }
});
