window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/locations/";
  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error("Failed to fetch locations url! Try again!");
    }

    const data = await response.json();

    const locationTag = document.getElementById("location");
    for (let location of data.locations) {
      const option = document.createElement("option");
      option.value = location.id;
      option.innerHTML = location.name;
      locationTag.appendChild(option);
    }

    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener("submit", async (event) => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const conferenceUrl = "http://localhost:8000/api/conferences/";
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newConference = await response.json();
          console.log(newConference);
        }
      } catch (e) {
        console.error("failed to make new conference!", e);
      }
    });
  } catch (e) {
    console.error("Failed to get location api!", e);
  }
});
