function createCard(
  title,
  description,
  pictureUrl,
  starts,
  ends,
  locationName
) {
  return `
  <div class="p-3 col-sm-4"
    <div class="card">
      <div class="shadow bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top" alt="">
          <div class="card-body mx-0 p-0 pt-2">
            <h5 class="card-title px-2">${title}</h5>
            <h6 class="card-subtitle p-2 text-muted">${locationName}</h6>
            <p class="card-text px-2">${description}</p>
            <div class="card-footer px-2 p-0">
              <p>${starts} - ${ends}</p>
            </div>
          </div>
      </div>
    </div>
  `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";
  const errorAlert = document.getElementById("error-alert");
  const errorMessageText = document.getElementById("error-message-text");

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error("Failed to fetch conferences! Try again!");
      errorMessageText.textContent = "Failed to fetch conferences! Try again!";
      errorAlert.style.display = "block";
      return;
    }
    const data = await response.json();

    for (let conference of data.conferences) {
      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);

      if (!detailResponse.ok) {
        console.error("Failed to fetch details for conference! Try again!");
        errorMessageText.textContent =
          "Failed to fetch details for conference! Try again!";
        errorAlert.style.display = "block";
      }

      if (detailResponse.ok) {
        const details = await detailResponse.json();

        const title = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const startDate = new Date(details.conference.starts);
        const starts = startDate.toLocaleDateString();
        const endDate = new Date(details.conference.ends);
        const ends = endDate.toLocaleDateString();
        const locationName = details.conference.location.name;
        const html = createCard(
          title,
          description,
          pictureUrl,
          starts,
          ends,
          locationName
        );

        const row = document.querySelector(".row");
        row.innerHTML += html;
      }
    }
  } catch (e) {
    console.error(e);
    errorMessageText.textContent =
      "Failed while fetching conferences! Try again!";
    errorAlert.style.display = "block";
  }
});
