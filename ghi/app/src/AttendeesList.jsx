import { useState, useEffect } from "react";

function AttendeesList() {
  const [attendees, setAttendees] = useState([])
  useEffect(() => {
    async function loadAttendees() {
      const url = 'http://localhost:8001/api/attendees/'

      try {
        const response = await fetch(url);

        if (!response.ok) {
          console.error("failed to fetch attendeeds api", response)
          return
        }

        if (response.ok) {
          const data = await response.json();
          setAttendees(data.attendees)
        }
      } catch (e) {
        console.error("failed to load attendees!", e)
      }
    }
    loadAttendees()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {attendees.map((attendee) => {
          return (
            <tr key={attendee.href}>
              <td>{attendee.name}</td>
              <td>{attendee.conference}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AttendeesList;
