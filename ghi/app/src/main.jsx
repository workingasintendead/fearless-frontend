import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import App from './App.jsx'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm"

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    // loader: rootLoader,
    children: [
      {
        index: true,
        element: <AttendeesList />,
        // loader: teamLoader,
      },
      {
        path: "conferences/new",
        element: <ConferenceForm />,
        // loader: teamLoader,
      },
      {
        path: "attendees/new",
        element: <AttendConferenceForm />,
        // loader: teamLoader,
      },
      {
        path: "locations/new",
        element: <LocationForm />,
        // loader: teamLoader,
      },
      {
        path: "attendees",
        element: <AttendeesList />,
        // loader: teamLoader,
      },
      {
        path: "*",
        element: (
          <main style={{ padding: "1rem" }}>
            <p>There&apos;s nothing here!</p>
          </main>
        ),
      },
    ],
  },
]);


// ReactDOM.createRoot(document.getElementById("root")).render(
//   <React.StrictMode>
//     <RouterProvider router={router} />
//   </React.StrictMode>
// );

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
