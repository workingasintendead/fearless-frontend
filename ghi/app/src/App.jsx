import Nav from './Nav';
import LocationForm from './LocationForm';
import { Outlet } from 'react-router-dom';


function App() {
  return (
    <>
      <Nav />
      <div className="container">
        <Outlet />
      </div>
    </>
  );
}

export default App;
